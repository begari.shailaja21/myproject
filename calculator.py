from cgitb import text
# The cgitb module provides a special exception handler for Python script
import operator
import string
from tkinter import*
# Here * means all the functions and modules comes here 
# function defing here
def btnClick(number):
    global operator
    operator=operator+str(number)
    text_Input.set(operator)

# here  btn clear function defing
def btnClear():
    global operator
    operator=""
    text_Input.set("")

# here  equal btn defing the function
def btnEqualDisaplay():
    global operator
    sumup=str(eval(operator))
    text_Input.set(sumup)


cal=Tk()
# here creatig the GUI appication in the main window means here (cal)
cal.title("calculator")
operator=""
text_Input=StringVar()
# Display the box
txtDispaly=Entry(cal,font=("arial",20),textvariable=text_Input,bd=10,insertwidth=4,bg="white",justify="right").grid(columnspan=4)

# Row number1
btn7=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="7",command=lambda:btnClick(7)).grid(row=1,column=0)

btn8=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="8",command=lambda:btnClick(8)).grid(row=1,column=1)

btn9=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="9",command=lambda:btnClick(9)).grid(row=1,column=2)

addition=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="+",command=lambda:btnClick("+")).grid(row=1,column=3)
# row number2
btn4=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="4",command=lambda:btnClick(4)).grid(row=2,column=0)

btn5=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="5",command=lambda:btnClick(5)).grid(row=2,column=1)

btn6=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="6",command=lambda:btnClick(6)).grid(row=2,column=2)

substration=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="-",command=lambda:btnClick("-")).grid(row=2,column=3)
# row number3
btn3=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="3",command=lambda:btnClick(3)).grid(row=3,column=0)

btn2=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="2",command=lambda:btnClick(2)).grid(row=3,column=1)

btn1=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="1",command=lambda:btnClick(1)).grid(row=3,column=2)

multipication=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="*",command=lambda:btnClick("*")).grid(row=3,column=3)
# row number 4
btn0=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="0",command=lambda:btnClick(0)).grid(row=4,column=0)

btnclear=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="C",command=btnClear).grid(row=4,column=1)

btnEqual=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="=",command=btnEqualDisaplay).grid(row=4,column=2)

btndivision=Button(cal,padx=16,bd=8,fg="black",font=("arial",20,"bold"),text="/",command=lambda:btnClick("/")).grid(row=4,column=3)





cal.mainloop()